package client

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"strings"
	"time"
)

const (
	Version = "0.1.0"
)

// Client is a KVaaS client.
type Client struct {
	targetURL string
	uuid      string
	token     string

	client *http.Client
}

// New creates a new KVaaS client.
func New(targetURL string, uuid string, token string) Client {
	return Client{
		targetURL: targetURL,
		uuid:      uuid,
		token:     token,

		client: &http.Client{
			Timeout: time.Second * 180,
			Transport: &http.Transport{
				MaxIdleConns:      10,
				DisableKeepAlives: true,
				IdleConnTimeout:   time.Second * 180,
			},
		},
	}
}

// request sends a HTTP request to the KVaaS server.
func (c *Client) request(method string, path string, body []byte) (*http.Response, error) {
	var reqBody io.Reader
	if body != nil {
		reqBody = bytes.NewReader(body)
	}

	// A new http request is created with the method, targetURL, uuid and path.
	// The request url should always be https://<domain>/<uuid>/<path>. Domain
	// may contain a slash at the end which may need to be stripped.
	if strings.HasSuffix(c.targetURL, "/") {
		c.targetURL = strings.TrimSuffix(c.targetURL, "/")
	}
	if strings.HasPrefix(path, "/") {
		path = strings.TrimPrefix(path, "/")
	}

	req, err := http.NewRequest(method, fmt.Sprintf("%s/%s/%s", c.targetURL, c.uuid, path), reqBody)
	if err != nil {
		return nil, err
	}

	req.Header.Set("Authorization", fmt.Sprintf("Bearer %s", c.token))
	req.Header.Set("User-Agent", "KVaaS-Client/"+Version)
	req.Header.Set("Accept", "application/json,text/plain,application/octet-stream")
	req.Header.Set("Cache-Control", "no-cache")

	return c.client.Do(req)
}

// Get retrieves a file from the KVaaS server.
func (c *Client) Get(file string) ([]byte, error) {
	resp, err := c.request("GET", file, nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("GET %s: %s", file, body)
	}

	return body, nil
}

// Put stores a file on the KVaaS server.
func (c *Client) Put(file string, body []byte) error {
	resp, err := c.request("PUT", file, body)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusNoContent {
		return fmt.Errorf("PUT %s: %s", file, resp.Status)
	}

	return nil
}

// Delete removes a file from the KVaaS server.
func (c *Client) Delete(file string) error {
	resp, err := c.request("DELETE", file, nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	if resp.StatusCode != http.StatusNoContent {
		return fmt.Errorf("DELETE %s: %s", file, resp.Status)
	}

	return nil
}

// List retrieves a list of files from the KVaaS server.
func (c *Client) List() ([]string, error) {
	resp, err := c.request("GET", "/", nil)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := io.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	if resp.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("LIST: %s", body)
	}

	var files []string
	err = json.Unmarshal(body, &files)
	if err != nil {
		return nil, err
	}

	return files, nil
}
