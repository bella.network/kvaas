# Key Value (Storage) As A Service (KVaaS)

This is a simple key value storage service that can be used to store and retrieve key value pairs. Users are seperated by their own assigned UUIDs with JWT authentication.

**Note:** This service is not meant to be used in production and is currently under heavy development. Some features may not be implemented yet, the API may change and the service may be unstable.

Aim of this project is to provide a simple key value storage service that can be used by developers to store and retrieve key value pairs. The service is meant to be used as a simple storage service for small projects and should not be used in production.

As an example, my current use case is to store a preferences file to be served to GitLab CI/CD pipelines. This preferences file is stored in the key value store and is retrieved by the CI/CD pipeline to be used in the build process.

## API Endpoints

The API is a RESTful API with the defined endpoints below. All requests must be authenticated with a JWT token in the `Authorization` header.
```
GET /{UUID}/{key}
Authorization: Bearer {JWT}
```

### GET /{UUID}/{key}

This endpoint is used to retrieve a value from the key value store. The UUID is the user's unique identifier and the key is the key of the key value pair.

```go
package main

import (
	kvaas "gitlab.com/bella.network/kvaas/pkg/client"
)

func main() {
	kv := kvaas.New(
		"https://kvaas.bella.network", // KVaaS API URL
		"ded6d463-2e37-47c7-8751-19bbeafda53e", // UUID
		"...", // JWT Token
	)

	value, err := kv.Get("key")
	if err != nil {
		panic(err)
	}

	fmt.Println(value)
}
```

### PUT /{UUID}/{key}

This endpoint is used to store a key value pair. The UUID is the user's unique identifier and the key is the key of the key value pair. The value is passed in the request body.

### DELETE /{UUID}/{key}

This endpoint is used to delete a key value pair from the store. The UUID is the user's unique identifier and the key is the key of the key value pair.
