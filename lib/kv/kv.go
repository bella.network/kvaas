package kv

type KV struct {
	storagePrimary Storage
}

func New(primaryStorage Storage) KV {
	return KV{
		storagePrimary: primaryStorage,
	}
}

func (k *KV) Get(key string) ([]byte, error) {
	return k.storagePrimary.Get(key)
}

func (k *KV) Set(key string, value []byte) error {
	return k.storagePrimary.Set(key, value)
}

func (k *KV) Delete(key string) error {
	return k.storagePrimary.Delete(key)
}

func (k *KV) List(prefix string) ([]string, error) {
	return k.storagePrimary.List(prefix)
}

func (k *KV) Metadata(key string) (map[string]string, error) {
	return k.storagePrimary.Metadata(key)
}

func (k *KV) Close() error {
	return k.storagePrimary.Close()
}
