package kv

import (
	"io"
	"os"
)

type StorageDisk struct {
	directory string
}

func (s *StorageDisk) Get(key string) ([]byte, error) {
	file, err := os.Open(s.directory + "/" + key)
	if err != nil {
		return nil, err
	}
	defer file.Close()

	body, err := io.ReadAll(file)
	if err != nil {
		return nil, err
	}

	return body, nil
}

func (s *StorageDisk) Set(key string, value []byte) error {
	// key is most probably UUID/filename, create UUID directory if it doesn't exist
	// get string until first /
	if len(key) < 36 {
		return os.ErrInvalid
	}

	uuid := key[:36]
	err := os.MkdirAll(s.directory+"/"+uuid, 0755)
	if err != nil {
		return err
	}

	file, err := os.Create(s.directory + "/" + key)
	if err != nil {
		return err
	}
	defer file.Close()

	_, err = file.Write(value)
	if err != nil {
		return err
	}

	return nil
}

func (s *StorageDisk) Delete(key string) error {
	err := os.Remove(s.directory + "/" + key)
	if err != nil {
		return err
	}

	return nil
}

func (s *StorageDisk) Close() error {
	return nil
}

// List implements Storage.
func (s *StorageDisk) List(prefix string) ([]string, error) {
	panic("unimplemented")
}

// Metadata implements Storage.
func (s *StorageDisk) Metadata(key string) (map[string]string, error) {
	panic("unimplemented")
}

func NewStorageDisk(path string) *StorageDisk {
	return &StorageDisk{
		directory: path,
	}
}
