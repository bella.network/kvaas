package kv

type Storage interface {
	// Get gets the value for the given key.
	Get(key string) ([]byte, error)

	// Set sets the value for the given key.
	Set(key string, value []byte) error

	// Metadata gets the metadata for the given key.
	Metadata(key string) (map[string]string, error)

	// Delete deletes the value for the given key.
	Delete(key string) error

	// List lists all keys.
	List(prefix string) ([]string, error)

	// Close closes the storage.
	Close() error
}
