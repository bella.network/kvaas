package auth

import (
	"errors"
	"time"

	"github.com/golang-jwt/jwt/v5"
	"github.com/google/uuid"
)

type Auth struct {
	secret []byte
}

func New(secret []byte) Auth {
	return Auth{
		secret: secret,
	}
}

func (a *Auth) GenerateJWT(uuid string) (string, error) {
	token := jwt.NewWithClaims(jwt.SigningMethodHS256,
		jwt.MapClaims{
			"sub": uuid,
			"iat": time.Now().Unix(),
		},
	)

	tokenString, err := token.SignedString(a.secret)
	if err != nil {
		return "", err
	}

	return tokenString, nil
}

func (a *Auth) ValidateJWT(tokenString string) (string, error) {
	token, err := jwt.Parse(tokenString, func(token *jwt.Token) (interface{}, error) {
		return a.secret, nil
	})
	if err != nil {
		return "", err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !ok || !token.Valid {
		return "", ErrUnauthorized
	}

	uuid, ok := claims["sub"].(string)
	if !ok {
		return "", ErrUnauthorized
	}

	return uuid, nil
}

var ErrUnauthorized = errors.New("unauthorized")

func GenerateUUID() string {
	id := uuid.New()
	return id.String()
}
