package api

import (
	"fmt"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func (a *API) get(w http.ResponseWriter, r *http.Request) {
	// Validate the request
	if !a.validateAPIRequestAuthorization(r) {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		log.Printf("HTTP:GET:%s: Unauthorized for %s", r.URL.Path, r.RemoteAddr)
		return
	}

	// Get the UUID from the request
	vars := mux.Vars(r)
	uuid := vars["uuid"]
	key := vars["key"]

	// Validate key
	if !validatePath(key) {
		http.Error(w, "Invalid key", http.StatusBadRequest)
		log.Printf("HTTP:GET:%s: Invalid key for %s", r.URL.Path, r.RemoteAddr)
		return
	}

	// Get the value
	value, err := a.kvs.Get(fmt.Sprintf(
		"%s/%s",
		uuid,
		key,
	))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Printf("HTTP:GET:%s: %s for %s", r.URL.Path, err.Error(), r.RemoteAddr)
		return
	}

	// Return the value
	log.Printf("HTTP:GET:%s: OK for %s", r.URL.Path, r.RemoteAddr)
	w.WriteHeader(http.StatusOK)
	w.Write(value)
}
