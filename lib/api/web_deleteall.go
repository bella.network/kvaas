package api

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func (a *API) deleteall(w http.ResponseWriter, r *http.Request) {
	// Validate the request
	if !a.validateAPIRequestAuthorization(r) {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	// Get the UUID from the request
	vars := mux.Vars(r)
	uuid := vars["uuid"]

	// Delete all values
	keys, err := a.kvs.List(uuid)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	for _, key := range keys {
		err = a.kvs.Delete(fmt.Sprintf(
			"%s/%s",
			uuid,
			key,
		))
		if err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}

	// Return the status
	w.WriteHeader(http.StatusNoContent)
}
