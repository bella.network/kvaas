package api

import (
	"encoding/json"
	"net/http"

	"github.com/gorilla/mux"
)

func (a *API) metadata(w http.ResponseWriter, r *http.Request) {
	// Validate the request
	if !a.validateAPIRequestAuthorization(r) {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	// Get the UUID from the request
	vars := mux.Vars(r)
	uuid := vars["uuid"]

	// Get the metadata
	metadata, err := a.kvs.Metadata(uuid)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Return the metadata
	w.WriteHeader(http.StatusOK)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(metadata)
}
