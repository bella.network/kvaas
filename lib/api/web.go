package api

import (
	"net/http"
	"strings"
	"time"

	"github.com/gorilla/mux"
	"gitlab.com/bella.network/kvaas/lib/auth"
	"gitlab.com/bella.network/kvaas/lib/kv"
)

// API implements the API. Ressources are provided by RESTful API in the form of
// /{uuid}/{key}. Every request is authenticated by the Authorization header
// which includes a JWT token that includes the associated UUID.

type API struct {
	kvs  kv.KV
	auth auth.Auth
}

func New() *API {
	return &API{}
}

func (a *API) SetKV(kvs kv.KV) {
	a.kvs = kvs
}

func (a *API) SetAuth(auth auth.Auth) {
	a.auth = auth
}

func (a *API) Start() error {
	r := mux.NewRouter()
	r.HandleFunc("/{uuid}/", a.list).Methods("GET")
	r.HandleFunc("/{uuid}/", a.deleteall).Methods("DELETE")
	r.HandleFunc("/{uuid}/{key}", a.get).Methods("GET")
	r.HandleFunc("/{uuid}/{key}", a.set).Methods("PUT", "POST")
	r.HandleFunc("/{uuid}/{key}", a.delete).Methods("DELETE")
	r.HandleFunc("/{uuid}/{key}", a.metadata).Methods("HEAD")

	srv := &http.Server{
		Handler:      r,
		Addr:         "0.0.0.0:8412",
		WriteTimeout: 15 * time.Second,
		ReadTimeout:  15 * time.Second,
	}

	return srv.ListenAndServe()
}

func (a *API) validateAPIRequestAuthorization(r *http.Request) bool {
	// The client must provide a JWT token in the Authorization header. The
	// token is validated and the UUID is returned.
	authHeader := r.Header.Get("Authorization")
	if authHeader == "" {
		return false
	}

	// The token is expected to be in the form of "Bearer <token>".
	// The token is extracted and validated.
	if len(authHeader) < 8 || authHeader[:7] != "Bearer " {
		return false
	}

	tokenString := authHeader[7:]
	uuid, err := a.auth.ValidateJWT(tokenString)
	if err != nil {
		return false
	}

	// In the request /{uuid}/, the UUID must match the UUID in the token.
	vars := mux.Vars(r)
	return vars["uuid"] == uuid
}

// validatePath validates the path if it's not a directory traversal attack.
func validatePath(path string) bool {
	// The path must not contain ".." to prevent directory traversal attacks.
	return !strings.Contains(path, "..")
}
