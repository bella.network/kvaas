package api

import (
	"fmt"
	"io"
	"log"
	"net/http"

	"github.com/gorilla/mux"
)

func (a *API) set(w http.ResponseWriter, r *http.Request) {
	// Validate the request
	if !a.validateAPIRequestAuthorization(r) {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		log.Printf("HTTP:SET:%s: Unauthorized for %s", r.URL.Path, r.RemoteAddr)
		return
	}

	// Get the UUID from the request
	vars := mux.Vars(r)
	uuid := vars["uuid"]
	key := vars["key"]

	// Validate key
	if !validatePath(key) {
		http.Error(w, "Invalid key", http.StatusBadRequest)
		log.Printf("HTTP:SET:%s: Invalid key for %s", r.URL.Path, r.RemoteAddr)
		return
	}

	// Body contains the value which should be stored.
	body, err := io.ReadAll(r.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Printf("HTTP:SET:%s: %s for %s", r.URL.Path, err.Error(), r.RemoteAddr)
		return
	}

	// Store the value
	err = a.kvs.Set(fmt.Sprintf(
		"%s/%s",
		uuid,
		key,
	), body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		log.Printf("HTTP:SET:%s: %s for %s", r.URL.Path, err.Error(), r.RemoteAddr)
		return
	}

	// Return the status
	w.WriteHeader(http.StatusNoContent)
	log.Printf("HTTP:SET:%s: OK for %s", r.URL.Path, r.RemoteAddr)
}
