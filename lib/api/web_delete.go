package api

import (
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func (a *API) delete(w http.ResponseWriter, r *http.Request) {
	// Validate the request
	if !a.validateAPIRequestAuthorization(r) {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	// Get the UUID from the request
	vars := mux.Vars(r)
	uuid := vars["uuid"]
	key := vars["key"]

	// Validate key
	if !validatePath(key) {
		http.Error(w, "Invalid key", http.StatusBadRequest)
		return
	}

	// Delete the value
	err := a.kvs.Delete(fmt.Sprintf(
		"%s/%s",
		uuid,
		key,
	))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Return the status
	w.WriteHeader(http.StatusNoContent)
}
