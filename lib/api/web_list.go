package api

import (
	"encoding/json"
	"fmt"
	"net/http"

	"github.com/gorilla/mux"
)

func (a *API) list(w http.ResponseWriter, r *http.Request) {
	// Validate the request
	if !a.validateAPIRequestAuthorization(r) {
		http.Error(w, "Unauthorized", http.StatusUnauthorized)
		return
	}

	// Get the UUID from the request
	vars := mux.Vars(r)
	uuid := vars["uuid"]
	prefix := vars["prefix"]

	// List the values
	keys, err := a.kvs.List(fmt.Sprintf(
		"%s/%s",
		uuid,
		prefix,
	))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}

	// Return json list of keys
	type keysResponse struct {
		Count int      `json:"count"`
		Files []string `json:"files"`
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(keysResponse{
		Count: len(keys),
		Files: keys,
	})
}
